cmake_minimum_required(VERSION 3.0)
project(model CXX)

set(SOURCE
    Model.cpp
    Model.h
)

UTILS_ADD_LIBRARY(model ${SOURCE})

target_link_libraries(model geometry)
target_include_directories(model PUBLIC
    ${geometry_SOURCE_DIR}
)
