#include "Model.h"

using geometry::Vec3f;

namespace model {

size_t Model::nfaces() const
{
    return faces.size();
}

size_t Model::nverts() const
{
    return verts.size();
}

const std::vector<size_t> &Model::face(size_t i) const
{
    return faces[i];
}

Vec3f Model::vert(size_t i) const
{
    return verts[i];
}

}
