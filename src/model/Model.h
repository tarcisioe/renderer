#ifndef _MODEL_H_
#define _MODEL_H_

#include <vector>

#include "Geometry.h"

namespace model {

class Model {
public:
    Model(std::vector<geometry::Vec3f> verts, std::vector<std::vector<std::size_t>> faces):
        verts{move(verts)},
        faces{move(faces)}
    {}
    std::size_t nverts() const;
    std::size_t nfaces() const;
    geometry::Vec3f vert(std::size_t i) const;
    const std::vector<std::size_t> &face(std::size_t i) const;

private:
    std::vector<geometry::Vec3f> verts;
    std::vector<std::vector<std::size_t>> faces;
};

}

#endif
