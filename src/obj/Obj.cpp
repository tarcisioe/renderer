#include "Model.h"

#include <string>
#include <fstream>
#include <sstream>

#include <iostream>

using geometry::Vec3f;
using model::Model;

namespace obj {

model::Model load(const std::string &filename)
{
    std::ifstream in{filename};

    auto line = std::string{};

    auto verts = std::vector<Vec3f>{};
    auto faces = std::vector<std::vector<size_t>>{};

    while (!in.eof()) {
        std::getline(in, line);
        std::istringstream iss{line};
        auto trash = char{};

        if (!line.compare(0, 2, "v ")) {
            iss >> trash;
            auto v = Vec3f{};
            for (auto i= 0u; i < 3; ++i) {
                iss >> v.raw[i];
            }
            verts.push_back(v);
        } else if (!line.compare(0, 2, "f ")) {
            auto f = std::vector<size_t>{};
            auto itrash = int{};
            auto idx = size_t{};
            iss >> trash;
            while (iss >> idx >> trash >> itrash >> trash >> itrash) {
                --idx;
                f.push_back(idx);
            }
            faces.push_back(f);
        }
    }

    return Model{move(verts), move(faces)};
}

}
