#ifndef _OBJ_H_
#define _OBJ_H_

#include <string>

#include "Model.h"

namespace obj {

model::Model load(const std::string &filename);

}

#endif
