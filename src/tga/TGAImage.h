#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <fstream>
#include <cstdint>

namespace tga {

#pragma pack(push,1)
struct TGA_Header {
    int8_t idlength;
    int8_t colormaptype;
    int8_t datatypecode;
    int16_t colormaporigin;
    int16_t colormaplength;
    int8_t colormapdepth;
    int16_t x_origin;
    int16_t y_origin;
    int16_t width;
    int16_t height;
    int8_t  bitsperpixel;
    int8_t  imagedescriptor;
};
#pragma pack(pop)

struct TGAColor {
    union {
        struct {
            uint8_t b, g, r, a;
        };
        uint8_t raw[4];
        uint32_t val;
    };
    size_t bytespp;

    constexpr TGAColor():
        val{0},
        bytespp{1}
    {}

    constexpr TGAColor(uint8_t R, uint8_t G, uint8_t B, uint8_t A):
        b{B},
        g{G},
        r{R},
        a{A},
        bytespp{4}
    {}

    constexpr TGAColor(uint32_t v, size_t bpp):
        val{v},
        bytespp{bpp}
    {}

    TGAColor(const uint8_t *p, size_t bpp);
};


class TGAImage {
public:
    enum Format {
        GRAYSCALE=1, RGB=3, RGBA=4
    };

    TGAImage();
    TGAImage(int w, int h, int bpp);
    TGAImage(const TGAImage &img);
    bool read_tga_file(const char *filename);
    bool write_tga_file(const char *filename, bool rle=true);
    bool flip_horizontally();
    bool flip_vertically();
    bool scale(int w, int h);
    TGAColor get(int x, int y);
    bool set(int x, int y, TGAColor c);
    ~TGAImage();
    TGAImage & operator =(const TGAImage &img);
    int get_width();
    int get_height();
    int get_bytespp();
    unsigned char *buffer();

private:
    unsigned char* data;
    int width;
    int height;
    int bytespp;

    bool   load_rle_data(std::ifstream &in);
    bool unload_rle_data(std::ofstream &out);
    void clear();
};

}

#endif //__IMAGE_H__
