#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "Buffer.h"
#include "Geometry.h"

namespace tga {
    class TGAColor;
    class TGAImage;
}

namespace tools {
    void line(
        const geometry::Vec3i &t0,
        const geometry::Vec3i &t1,
        tga::TGAImage &image,
        Buffer<int> &buffer,
        const tga::TGAColor &color
    );

    void triangle(
        geometry::Vec3i t0,
        geometry::Vec3i t1,
        geometry::Vec3i t2,
        tga::TGAImage &image,
        Buffer<int> &buffer,
        const tga::TGAColor &color
    );
}

#endif
