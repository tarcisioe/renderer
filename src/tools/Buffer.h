#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <vector>

namespace tools {

template <typename T>
class Buffer {
public:
    Buffer(std::size_t width, std::size_t height):
        buffer(width*height),
        width{width},
        height{height}
    {
    }

    Buffer(std::size_t width, std::size_t height, T initial):
        Buffer{width, height}
    {
        for (auto x = 0u; x < height; ++x) {
            for (auto y = 0u; y < width; ++y) {
                (*this)(x, y) = initial;
            }
        }
    }

    T operator()(std::size_t x, std::size_t y) const
    {
        return buffer[y*width + x];
    }

    T &operator()(std::size_t x, std::size_t y)
    {
        return buffer[y*width + x];
    }

private:
    std::vector<T> buffer;
    std::size_t width, height;
};

}

#endif
