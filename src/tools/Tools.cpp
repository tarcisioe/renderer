#include "Tools.h"

#include <algorithm>

#include "Geometry.h"
#include "TGAImage.h"

namespace tools {

void line(
    const geometry::Vec3i &t0,
    const geometry::Vec3i &t1,
    tga::TGAImage &image,
    Buffer<int> &zbuffer,
    const tga::TGAColor &color
)
{
    using std::swap;

    auto x0 = t0.x;
    auto y0 = t0.y;
    auto x1 = t1.x;
    auto y1 = t1.y;

    auto steep = std::abs(x0-x1) < std::abs(y0-y1);

    if (steep) { // if the line is steep, we transpose the image
        swap(x0, y0);
        swap(x1, y1);
    }

    if (x0 > x1) { // make it left-to-right
        swap(x0, x1);
        swap(y0, y1);
    }

    auto dx = x1 - x0;
    auto dy = y1 - y0;
    auto derror = std::abs(dy)*2;
    auto error = 0;
    auto y = y0;

    for (auto x = x0; x <= x1; ++x) {
        if (steep) {
            image.set(y, x, color);
        } else {
            image.set(x, y, color);
        }

        error += derror;

        if (error > dx) {
            y += (y1 > y0 ? 1 : -1);
            error -= dx*2;
        }
    }
}

void triangle(
    geometry::Vec3i t0,
    geometry::Vec3i t1,
    geometry::Vec3i t2,
    tga::TGAImage &image,
    Buffer<int> &zbuffer,
    const tga::TGAColor &color
)
{
    using geometry::Vec3i;
    using geometry::Vec3f;

    // Discard degenerate triangles (all point on the same height)
    if (t0.y == t1.y && t0.y == t2.y) { return; }

    // Sort vertices by y (there are only three, so we bubble).
    if (t1.y < t0.y) { std::swap(t0, t1); }
    if (t2.y < t1.y) { std::swap(t1, t2); }
    if (t1.y < t0.y) { std::swap(t0, t1); }

    // Since they are sorted, t2 is the uppermost, t0 the lowermost.
    auto total_height = t2.y - t0.y;

    for (auto i = 0; i <= total_height; ++i) {
        // We know we are on the second half if we are still below
        // t1, or in case both t1 and t0 are at the same height.
        // (No halves on this case, actually).
        auto second_half = i > (t1.y - t0.y) || t1.y == t0.y;

        auto segment_height = second_half ? (t2.y - t1.y) : (t1.y - t0.y);

        auto alpha = double(i)/total_height;
        auto beta = double(i - (second_half ? (t1.y - t0.y) : 0))/segment_height;

        auto a = t0 + Vec3f{t2 - t0}*alpha;
        auto b = second_half ?
            t1 + Vec3f{t2 - t1}*beta:
            t0 + Vec3f{t1 - t0}*beta;

        if (b.x < a.x) { std::swap(a, b); }

        for (auto j = a.x; j < b.x; ++j) {
            auto phi = b.x == a.x ? 1. : double(j - a.x)/double(b.x - a.x);
            auto p = Vec3f{a} + Vec3f{b-a}*phi;
            if (zbuffer(p.x, p.y) < p.z) {
                zbuffer(p.x, p.y) = p.z;
                image.set(j, t0.y + i, color);
            }
        }
    }
}


}
