#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#include <cmath>
#include <ostream>

namespace geometry {

template <typename T>
struct Vec2 {
    union {
        struct {
            T u, v;
        };
        struct {
            T x, y;
        };
        T raw[2];
    };

    Vec2():
        u{},
        v{}
    {}

    Vec2(T u, T v):
        u{u},
        v{v}
    {}

    Vec2 operator+(const Vec2 &other) const { return Vec2{u+other.u, v+other.v}; }
    Vec2 operator-(const Vec2 &other) const { return Vec2{u-other.u, v-other.v}; }
    Vec2 operator*(double s) const { return Vec2{T(u*s), T(v*s)}; }
};

template <typename T>
struct Vec3 {
    union {
        struct {
            T x, y, z;
        };
        struct {
            T ivert, iuv, inorm;
        };
        T raw[3];
    };

    Vec3():
        x{},
        y{},
        z{}
    {}

    Vec3(T x, T y, T z):
        x{x},
        y{y},
        z{z}
    {}

    template <typename u>
    Vec3(const Vec3<u> &);


    Vec3 operator^(const Vec3 &other) const
    {
        return Vec3{y*other.z - z*other.y,
                    z*other.x - x*other.z,
                    x*other.y - y*other.x};
    }

    Vec3 operator+(const Vec3 &other) const
    {
        return Vec3{x+other.x,
                    y+other.y,
                    z+other.z};
    }

    Vec3 operator-(const Vec3 &other) const
    {
        return Vec3{x-other.x,
                    y-other.y,
                    z-other.z};
    }

    Vec3 operator*(double s) const { return Vec3{T(x*s), T(y*s), T(z*s)}; }

    T operator*(const Vec3 &other)
    {
        return x*other.x + y*other.y + z*other.z;
    }

    double norm() const { return std::sqrt(x*x+y*y+z*z); }

    Vec3 &normalize(T l=1) {
        *this = (*this)*(l/norm());
        return *this;
    }
};

using Vec2f = Vec2<double>;
using Vec2i = Vec2<int>;
using Vec3f = Vec3<double>;
using Vec3i = Vec3<int>;

template <typename T> std::ostream &operator<<(std::ostream &s, const Vec2<T> &v)
{
    s << "(" << v.x << ", " << v.y << ")\n";
    return s;
}

template <typename T> std::ostream &operator<<(std::ostream &s, const Vec3<T> &v)
{
    s << "(" << v.x << ", " << v.y << ", " << v.z << ")\n";
    return s;
}

}

#endif
