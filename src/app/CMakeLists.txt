cmake_minimum_required(VERSION 3.0)
project(app CXX)

set(SOURCE
    main.cpp
)

UTILS_ADD_EXECUTABLE(app ${SOURCE})

target_link_libraries(app obj render)
target_include_directories(app PUBLIC
    ${obj_SOURCE_DIR}
    ${render_SOURCE_DIR}
)
