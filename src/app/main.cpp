#include <iostream>
#include <array>
#include <random>

#include "Obj.h"
#include "RenderTGA.h"

constexpr size_t width = 800;
constexpr size_t height = 800;
constexpr size_t depth = 255;

int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cerr << "Please provide a filename.\n";
        return -1;
    }

    auto model = obj::load(argv[1]);

    auto image = render::render_tga(model, width, height, depth);

    image.write_tga_file("output.tga");
    return 0;
}

