cmake_minimum_required(VERSION 3.0)
project(opengl_clone CXX)

set(CMAKE_MODULE_PATH
    "${CMAKE_SOURCE_DIR}/cmake"
    "${CMAKE_SOURCE_DIR}/cmake-utils")

include(utils)

enable_testing()

if(PROFILE)
    if(CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "-pg")
        set(CMAKE_C_FLAGS "-pg")
        set(CMAKE_EXE_FLAGS "-pg")
    endif()
endif()


if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -flto")
endif()

add_subdirectory(geometry)
add_subdirectory(tga)
add_subdirectory(model)
add_subdirectory(obj)
add_subdirectory(tools)
add_subdirectory(render)
add_subdirectory(app)

foreach(_target ${TARGETS})
    set_property(TARGET ${_target} PROPERTY CXX_STANDARD 14)
endforeach()

UTILS_GENERATE_CLANG_COMPLETE()
