#include "RenderTGA.h"

#include <array>
#include <limits>

#include "Buffer.h"
#include "Geometry.h"
#include "Model.h"
#include "Tools.h"

namespace render {

tga::TGAImage render_tga(const model::Model &model, int width, int height, int depth)
{
    using namespace tga;
    using namespace tools;
    using namespace geometry;

    auto image = TGAImage{width, height, TGAImage::RGB};
    auto zbuffer = Buffer<int>{size_t(width), size_t(height), std::numeric_limits<int>::infinity()};

    auto light_dir = Vec3f{0, 0, -1};

    for (auto i = 0u; i < model.nfaces(); ++i) {
        const auto& face = model.face(i);

        std::array<Vec3i, 3> screen;
        std::array<Vec3f, 3> world;

        for (auto j = 0; j < 3; ++j) {
            Vec3f v = model.vert(face[j]);
            screen[j] = Vec3i{
                int((v.x + 1) * width/2),
                int((v.y + 1) * height/2),
                int((v.z + 1) * depth/2)
            };
            world[j] = v;
        }

        auto n = (world[2] - world[0])^(world[1] - world[0]);
        n.normalize();

        auto intensity = n*light_dir;

        if (intensity > 0) {
            triangle(
                screen[0],
                screen[1],
                screen[2],
                image,
                zbuffer,
                TGAColor{
                    uint8_t(intensity*255),
                    uint8_t(intensity*255),
                    uint8_t(intensity*255),
                    255
                }
            );
        }
    }

    image.flip_vertically(); // i want to have the origin at the left bottom corner of the image

    return image;
}

}
