#ifndef _RENDER_H_
#define _RENDER_H_

#include "TGAImage.h"

namespace model {
    class Model;
}

namespace render {

tga::TGAImage render_tga(const model::Model &model, int height, int width, int depth);

}

#endif
